import React, { Component } from "react";
import classNames from "classnames";

class GheItem extends Component {
  state = {
    isBooking: false,
    TrangThai: false,
  };
  renderChair = () => {
    this.props.addToCart(this.props.chairsList);
    this.setState({
      isBooking: !this.state.isBooking,
    });
  };
  render() {
    return (
      <div>
        <button
          style={{ width: 50 , height:50}}
          className={classNames(
            "btn p-1",
            { "btn-secondary": !this.state.isBooking },
            { "btn-success": this.state.isBooking },
            { "btn-danger": this.props.chairsList.TrangThai }
          )}
          onClick={this.renderChair}
          disabled={this.props.chairsList.TrangThai}
        >
          {this.props.chairsList.TenGhe}
        </button>
      </div>
    );
  }
}

export default GheItem;
