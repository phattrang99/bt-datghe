import React, { Component } from "react";
import GheItem from "./gheitem";

class DanhSachGhe extends Component {
  bookChair = () => {
    this.props.bookChair(this.props.chairsList);
  };
  renderChairs = () => {
    return this.props.chairsList.map((item) => {
      return (
        <div className="col-3 mt-2">
          <GheItem
            setSelectedChair={this.props.setSelectedChair}
            chairsList={item}
            addToCart={this.props.addToCart}
            danhSachGheDaDat={this.props.danhSachGheDaDat}
          />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row p">{this.renderChairs()}</div>
        <button className="btn btn-success mt-2" onClick={this.bookChair}>
          Chọn ghế
        </button>
      </div>
    );
  }
}

export default DanhSachGhe;
