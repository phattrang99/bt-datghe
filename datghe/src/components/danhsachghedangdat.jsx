import React, { Component } from "react";

class DanhSachGheDangDat extends Component {
  showChair = () => {
    this.props.bookChair(this.props.chairsList);
  };
  renderChair = () => {
    return this.props.danhSachGheDaDat.map((item) => {
      return (
        <p>
          Ghế: {item.product.TenGhe}${item.product.Gia}
        </p>
      );
    });
  };
  render() {
    return (
      <div>
        <h4 className="text-warning">Danh sách ghế đang đặt ({this.props.totalItem})</h4>
        {this.renderChair()}
      </div>
    );
  }
}

export default DanhSachGheDangDat;
