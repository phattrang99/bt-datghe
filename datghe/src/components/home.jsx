/* eslint-disable no-undef */
import React, { Component } from "react";
import DanhSachGhe from "./danhsachghe";
import DanhSachGheDangDat from "./danhsachghedangdat";

class Home extends Component {
  chairsList = [
    { SoGhe: 1, TenGhe: "số 1 ", Gia: 100, TrangThai: false },
    { SoGhe: 2, TenGhe: "số 2 ", Gia: 100, TrangThai: false },
    { SoGhe: 3, TenGhe: "số 3", Gia: 100, TrangThai: false },
    { SoGhe: 4, TenGhe: "số 4 ", Gia: 100, TrangThai: false },
    { SoGhe: 5, TenGhe: "số 5 ", Gia: 100, TrangThai: false },
    { SoGhe: 6, TenGhe: "số 6 ", Gia: 100, TrangThai: false },
    { SoGhe: 7, TenGhe: "số 7 ", Gia: 100, TrangThai: false },
    { SoGhe: 8, TenGhe: "số 8 ", Gia: 100, TrangThai: false },
    { SoGhe: 9, TenGhe: "số 9 ", Gia: 100, TrangThai: false },
    { SoGhe: 10, TenGhe: "số 10 ", Gia: 100, TrangThai: false },
    { SoGhe: 11, TenGhe: "số 11 ", Gia: 100, TrangThai: false },
    { SoGhe: 12, TenGhe: "số 12 ", Gia: 100, TrangThai: false },
    { SoGhe: 13, TenGhe: "số 13 ", Gia: 100, TrangThai: false },
    { SoGhe: 14, TenGhe: "số 14 ", Gia: 100, TrangThai: false },
    { SoGhe: 15, TenGhe: "số 15 ", Gia: 100, TrangThai: false },
    { SoGhe: 16, TenGhe: "số 16 ", Gia: 100, TrangThai: false },
    { SoGhe: 17, TenGhe: "số 17 ", Gia: 100, TrangThai: false },
    { SoGhe: 18, TenGhe: "số 18 ", Gia: 100, TrangThai: false },
    { SoGhe: 19, TenGhe: "số 19 ", Gia: 100, TrangThai: false },
    { SoGhe: 20, TenGhe: "số 20 ", Gia: 100, TrangThai: false },
    { SoGhe: 21, TenGhe: "số 21 ", Gia: 100, TrangThai: false },
    { SoGhe: 22, TenGhe: "số 22 ", Gia: 100, TrangThai: false },
    { SoGhe: 23, TenGhe: "số 23 ", Gia: 100, TrangThai: false },
    { SoGhe: 24, TenGhe: "số 24 ", Gia: 100, TrangThai: false },
    { SoGhe: 25, TenGhe: "số 25 ", Gia: 100, TrangThai: false },
    { SoGhe: 26, TenGhe: "số 26 ", Gia: 100, TrangThai: false },
    { SoGhe: 27, TenGhe: "số 27 ", Gia: 100, TrangThai: false },
    { SoGhe: 28, TenGhe: "số 28 ", Gia: 100, TrangThai: false },
    { SoGhe: 29, TenGhe: "số 29 ", Gia: 100, TrangThai: false },
    { SoGhe: 30, TenGhe: "số 30 ", Gia: 100, TrangThai: true },
    { SoGhe: 31, TenGhe: "số 31 ", Gia: 100, TrangThai: false },
    { SoGhe: 32, TenGhe: "số 32 ", Gia: 100, TrangThai: false },
    { SoGhe: 33, TenGhe: "số 33 ", Gia: 100, TrangThai: false },
    { SoGhe: 34, TenGhe: "số 34 ", Gia: 100, TrangThai: false },
    { SoGhe: 35, TenGhe: "số 35 ", Gia: 100, TrangThai: false },
    { SoGhe: 36, TenGhe: "số 36 ", Gia: 100, TrangThai: false },
  ];

  state = {
    selectedChair: null,
    danhSachGheDangDat: [],
    totalItem: 0,
    danhSachGheDaDat: [],
  };

  setSelectedChair = (chair) => {
    this.setState({
      selectedChair: chair,
    });
  };

  addToCart = (chair) => {
    const cloneCart = [...this.state.danhSachGheDangDat];

    const foundIndex = cloneCart.findIndex((item) => {
      return item.product.SoGhe === chair.SoGhe;
    });

    if (foundIndex === -1) {
      const gheItem = { product: chair };
      cloneCart.push(gheItem);
    } else {
      cloneCart.splice(foundIndex, 1);
    }
    this.setState(
      {
        danhSachGheDangDat: cloneCart,
      },
      () => {
        console.log(this.state.danhSachGheDangDat);
      }
    );
  };

  bookChair = (chair) => {
    let cloneChair = [...this.state.danhSachGheDangDat];
    let Amount = cloneChair.length;
    for (let item of cloneChair) {
      for (let item1 of chair) {
        if (item.product.SoGhe === item1.SoGhe) item1.TrangThai = true;
      }
    }
    this.setState(
      {
        totalItem: Amount,
        danhSachGheDaDat: cloneChair,
      },
      () => {
        console.log(chair);
      }
    );
  };

  render() {
    return (
      <div className="container text-center">
        <h1 className="text-warning">ĐẶT VÉ XE BUS HÃNG CYBERSOFT</h1>
        <div className="container">
          <div className="row d-flex justify-content-center">
            <div className="col-4">
              <h4 style={{backgroundColor: "#eae8e4" ,borderRadius: 5}}>Tài Xế</h4>
              <DanhSachGhe
                setSelectedChair={this.setSelectedChair}
                addToCart={this.addToCart}
                bookChair={this.bookChair}
                chairsList={this.chairsList}
                danhSachGheDaDat={this.state.danhSachGheDaDat}
              />
            </div>
            <div className="col-4">
              <DanhSachGheDangDat
                addToCart={this.addToCart}
                bookChair={this.bookChair}
                chairsList={this.chairsList}
                danhSachGheDaDat={this.state.danhSachGheDaDat}
                danhSachGheDangDat={this.state.danhSachGheDangDat}
                totalItem={this.state.totalItem}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
